import React, { Component } from 'react';

import '../index.css';


export default class counter extends Component {
	state = {
		value: this.props.value // 0-dirsa 'zero' yazilacaq eks halda deyeri gosterecek
		// imageUrl: 'https://picsum.photos/400',
		// tags: ['tag1','tag2','tag3']
	}

	// constructor(props) {
	// 	super();
	// 	this.handleIncriment = this.handleIncriment.bind(this); // +
	// 	this.minusIncriment = this.minusIncriment.bind(this); // -
	// }

	// +
	handleIncriment = () => {
		this.setState({ value: this.state.value + 1 });
		console.log('true', this);
	}

	// -
	minusIncriment = () => {
		var oldCount = this.state.value;
		// var element = document.getElementByClassName("myDIV");

		if(oldCount > 0){
			this.setState({ value: this.state.value - 1 });
			console.log('false', this);
		} else {
			// element.className('disable'); // disable class-i add olunmur
			console.log('sorry..');
		}
	}

	render() {
		return (
			<div>
				{/* <img src={this.state.imageUrl} /> */}
				<button 
					onClick={this.minusIncriment} 
					className="btn"> -
				</button>
				<span className={this.getBadgeClasses()}>{this.formatCount()}</span>
				<button 
					onClick={this.handleIncriment} 
					className="btn"> +
				</button>
				<button 
					onClick={() => this.props.onDelete(this.props.id)}
					className="btn btn-danger">
					Delete
				</button>
				{/*
					<ul>
						{ this.state.tags.map(tag => <li key={tag}>{tag}</li>) }
					</ul>
				*/}
			</div>
		);
	}

	getBadgeClasses() {
		let classes = "badge badge-";
		classes += (this.state.value === 0) ? "warning" : "primary";
		return classes;
	}

	formatCount(){
		const { value } = this.state;
		return value === 0 ? 'Zero' : value;
	}
}
